-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 12, 2019 at 03:19 AM
-- Server version: 5.7.22
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_koperasi`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_angsuran`
--

CREATE TABLE `tb_angsuran` (
  `no_trans` int(11) DEFAULT NULL,
  `id_modal` int(11) DEFAULT NULL,
  `id_pinjaman` int(11) DEFAULT NULL,
  `anguran_ke` int(11) DEFAULT NULL,
  `besar_angsuran` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_data_modal`
--

CREATE TABLE `tb_data_modal` (
  `id_modal` int(11) NOT NULL,
  `jumlah_modal_awal` double DEFAULT NULL,
  `jumlah_modal_sekarang` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_login`
--

CREATE TABLE `tb_login` (
  `id_user` int(11) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_nasabah`
--

CREATE TABLE `tb_nasabah` (
  `id_nasabah` int(11) NOT NULL,
  `id_petugas` int(11) DEFAULT NULL,
  `nama` varchar(70) DEFAULT NULL,
  `alamat_ktp` varchar(70) DEFAULT NULL,
  `alamat_sekarang` varchar(70) DEFAULT NULL,
  `tempat_lahir` varchar(70) DEFAULT NULL,
  `tgl_lahir` varchar(30) DEFAULT NULL,
  `jkel` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `nama_umkm` varchar(50) DEFAULT NULL,
  `foto_ktp` longblob,
  `foto_kk` longblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_petugas`
--

CREATE TABLE `tb_petugas` (
  `id_petugas` int(11) NOT NULL,
  `nama` varchar(70) DEFAULT NULL,
  `alamat` varchar(70) DEFAULT NULL,
  `no_telp` varchar(20) DEFAULT NULL,
  `tmpt_lahir` varchar(30) DEFAULT NULL,
  `tgl_lahir` varchar(30) DEFAULT NULL,
  `jabatan` varchar(30) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_petugas`
--

INSERT INTO `tb_petugas` (`id_petugas`, `nama`, `alamat`, `no_telp`, `tmpt_lahir`, `tgl_lahir`, `jabatan`, `updated_at`, `created_at`) VALUES
(1, 'andre', 'SDA', '8786776756', 'pbg', '10-06-1997', 'Mantri', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pinjaman`
--

CREATE TABLE `tb_pinjaman` (
  `id_pinjaman` int(11) NOT NULL,
  `id_anggota` int(11) DEFAULT NULL,
  `id_modal` int(11) DEFAULT NULL,
  `besar_penjaman` double DEFAULT NULL,
  `tgl_pengajuan` varchar(20) DEFAULT NULL,
  `tgl_pinjam` varchar(20) DEFAULT NULL,
  `lama_angsuran` int(11) DEFAULT NULL,
  `bunga` double DEFAULT NULL,
  `angsuran_perminggu` double DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `tgl_acc` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_angsuran`
--
ALTER TABLE `tb_angsuran`
  ADD KEY `tb_angsuran_tb_pinjaman_id_pinjaman_fk` (`id_pinjaman`),
  ADD KEY `tb_angsuran_tb_data_modal_id_modal_fk` (`id_modal`);

--
-- Indexes for table `tb_data_modal`
--
ALTER TABLE `tb_data_modal`
  ADD PRIMARY KEY (`id_modal`);

--
-- Indexes for table `tb_login`
--
ALTER TABLE `tb_login`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tb_nasabah`
--
ALTER TABLE `tb_nasabah`
  ADD PRIMARY KEY (`id_nasabah`);

--
-- Indexes for table `tb_petugas`
--
ALTER TABLE `tb_petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indexes for table `tb_pinjaman`
--
ALTER TABLE `tb_pinjaman`
  ADD PRIMARY KEY (`id_pinjaman`),
  ADD KEY `tb_pinjaman_tb_data_modal_id_modal_fk` (`id_modal`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_angsuran`
--
ALTER TABLE `tb_angsuran`
  ADD CONSTRAINT `tb_angsuran_tb_data_modal_id_modal_fk` FOREIGN KEY (`id_modal`) REFERENCES `tb_data_modal` (`id_modal`),
  ADD CONSTRAINT `tb_angsuran_tb_pinjaman_id_pinjaman_fk` FOREIGN KEY (`id_pinjaman`) REFERENCES `tb_pinjaman` (`id_pinjaman`);

--
-- Constraints for table `tb_nasabah`
--
ALTER TABLE `tb_nasabah`
  ADD CONSTRAINT `tb_nasabah_tb_petugas_id_petugas_fk` FOREIGN KEY (`id_nasabah`) REFERENCES `tb_petugas` (`id_petugas`);

--
-- Constraints for table `tb_pinjaman`
--
ALTER TABLE `tb_pinjaman`
  ADD CONSTRAINT `tb_pinjaman_tb_data_modal_id_modal_fk` FOREIGN KEY (`id_modal`) REFERENCES `tb_data_modal` (`id_modal`),
  ADD CONSTRAINT `tb_pinjaman_tb_nasabah_id_nasabah_fk` FOREIGN KEY (`id_pinjaman`) REFERENCES `tb_nasabah` (`id_nasabah`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
