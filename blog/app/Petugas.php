<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Petugas extends Model
{
    protected $table = 'tb_petugas';
    protected $fillable = ['id_petugas', 'nama', 'alamat', 'no_telp', 'tmpt_lahir', 'tgl_lahir', 'jabatan'];
    public $timestaps = false;
}


