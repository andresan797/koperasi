<?php

namespace App\Http\Controllers;

use App\M_Siswa;
use App\Petugas;
use Illuminate\Http\Request;

class Home extends Controller
{
    public function index() {
        return view('Dashboard');
    }
    public function formpetugas() {
        return view('formpegawai');
    }


    public function ajax_list() {
        $data = array();
        $list = Petugas::all();
        foreach ($list as $row) {
            $val = array();
            $val[] = $row->id_petugas;
            $val[] = $row->nama;
            $val[] = $row->alamat;
            $val[] = $row->no_telp;
            $val[] = $row->tmpt_lahir;
            $val[] = $row->tgl_lahir;
            $val[] = $row->jabatan;
            $val[] = '<div style="text-align: center;">'
                . '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Edit" onclick="ganti('."'".$row->id_petugas."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>&nbsp;'
                . '<a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$row->id_petugas."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>'
                . '</div>';
            $data[] = $val;
        }
        $output = array("data" => $data);
        echo json_encode($output);
    }

    public function simpanpegawai(Request $req) {

        $obj = new Petugas();
        $obj->id_petugas = $req->input('kode');
        $obj->nama = $req->input('nama');
        $obj->alamat = $req->input('alamat');
        $obj->no_telp = $req->input('notelp');
        $obj->tmpt_lahir = $req->input('tmpt_lahir');
        $obj->tgl_lahir = $req->input('tgl_lahir');
        $obj->jabatan = $req->input('jabatan');
        $simpan = $obj->save();
        if($simpan == 1){
            $status = "Tersmpan";
        }else{
            $status = "Gagal";
        }

        echo json_encode(array("status" => $status));
    }

}
