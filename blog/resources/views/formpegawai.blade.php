@extends('template')
@section('content')


    <script type="text/javascript">

        var save_method; //for save method string
        var table;

        $(document).ready(function() {
            table = $('#example').DataTable( {
                "ajax": "<?php echo url('/datapetugas') ?>"
            });
        });

        function reload(){
            table.ajax.reload(null,false); //reload datatable ajax
        }

        function add(){
            save_method = 'add';
            $('#form')[0].reset(); // reset form on modals
            $('#modal_form').modal('show'); // show bootstrap modal
            $('.modal-title').text('Tambah Pegawai'); // Set Title to Bootstrap modal title
            $('[name="kode"]').prop("readonly",false);
        }

        function save(){
            var url = "";
            if(save_method === 'add'){
                url = "<?php echo url('/simpanpegawai') ?>";
            }else{
                url = "<?php echo url('/updatesiswa') ?>";
            }
            $.ajax({
                url : url,
                type: "POST",
                data: $('#form').serialize(),
                dataType: "JSON",
                success: function(data)
                {
                    alert(data.status);
                    $('#modal_form').modal('hide');
                    reload();
                },
                error: function (jqXHR, textStatus, errorThrown){
                    alert("Error json " + errorThrown);
                }
            });
        }

        function ganti(id){

            save_method = 'update';
            $('#form')[0].reset(); // reset form on modals
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Ganti Siswa'); // Set title to Bootstrap modal title


            //Ajax Load data from ajax
            $.ajax({
                url : "<?php echo url('/'); ?>" + "/editsiswa/" + id,
                type: "GET",
                dataType: "JSON",
                success: function(data){
                    $('[name="kode"]').prop("readonly",true);

                    $('[name="kode"]').val(data.idsiswa);
                    $('[name="nama"]').val(data.nama);
                    $('[name="alamat"]').val(data.alamat);
                },error: function (jqXHR, textStatus, errorThrown){
                    alert('Error get data');
                }
            });
        }

        function hapus(id){
            if(confirm("Apakah anda yakin menghapus siswa dengan kode " + id + " ?")){
                // ajax delete data to database
                $.ajax({
                    url : "<?php echo url('/'); ?>" + "/delete/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function(data){
                        alert(data.status);
                        reload();
                    },error: function (jqXHR, textStatus, errorThrown){
                        alert('Error hapus data');
                    }
                });
            }
        }
    </script>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Simple Tables
                <small>preview of simple tables</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Tables</a></li>
                <li class="active">Simple</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <button class="btn btn-success" onclick="add();"><i class="glyphicon glyphicon-plus"></i> Add Siswa</button>
                            <button class="btn btn-default" onclick="reload();"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
                        </div>
                        <div class="box-body table-responsive no-padding">

                            <table id="example" class="display" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <th>Alamat</th>
                                    <th>Nomor Telepon</th>
                                    <th>Tempat Lahir</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Jabatan</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <th>Alamat</th>
                                    <th>Nomor Telepon</th>
                                    <th>Tempat Lahir</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Jabatan</th>
                                    <th>Aksi</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="modal fade" id="modal_form" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Form Petugas</h3>
                </div>
                <div class="modal-body form">
                    <form action="#" id="form" class="form-horizontal">
                        <input name="_token" type="hidden" id="_token" value="{{ csrf_token() }}" />
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-3">Kode</label>
                                <div class="col-md-9">
                                    <input name="kode" class="form-control" type="text" placeholder="Kode">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Nama</label>
                                <div class="col-md-9">
                                    <input name="nama" class="form-control" type="text" placeholder="Nama">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Alamat</label>
                                <div class="col-md-9">
                                    <input name="alamat" class="form-control" type="text" placeholder="Alamat">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">No Telpon</label>
                                <div class="col-md-9">
                                    <input name="nOtelp" class="form-control" type="text" placeholder="No Telpon">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tempat Lahir</label>
                                <div class="col-md-9">
                                    <input name="tmpt_lahir" class="form-control" type="text" placeholder="Tempat Lahir">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Tgl_Lahir</label>
                                <div class="col-md-9">
                                    <input name="tgl_lahir" class="form-control" type="text" placeholder="Tgl_Lahir">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Jabatan</label>
                                <div class="col-md-9">
                                    <input name="jabatan" class="form-control" type="text" placeholder="Jabatan">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnSave" onclick="save();" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@stop
