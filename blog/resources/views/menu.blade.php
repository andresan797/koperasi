<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        {{--<div class="user-panel">--}}
            {{--<div class="pull-left image">--}}
                {{--<img src="img/user2-160x160.jpg" class="img-circle" alt="User Image">--}}
            {{--</div>--}}
            {{--<div class="pull-left info">--}}
                {{--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>--}}
            {{--</div>--}}
        {{--</div>--}}
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="{{ url('/') }}"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span>DATA MASTER</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/') }}"><i class="fa fa-circle-o"></i>Form Nasabah</a></li>
                    <li><a href="{{ url('/formpegawai') }}"><i class="fa fa-circle-o"></i>Form Pegawai</a></li>
                    <li><a href="{{ url('/matkul') }}"><i class="fa fa-circle-o"></i>Form Data Modal</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span>Transaksi</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/') }}"><i class="fa fa-circle-o"></i>Form Pinjaman</a></li>
                    <li><a href="{{ url('/matkul') }}"><i class="fa fa-circle-o"></i>Form Transaksi</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span>Laporan</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('/') }}"><i class="fa fa-circle-o"></i>Laporan Kerugian</a></li>
                    <li><a href="{{ url('/matkul') }}"><i class="fa fa-circle-o"></i>Laporan Laba</a></li>
                    <li><a href="{{ url('/matkul') }}"><i class="fa fa-circle-o"></i>Laporan Uang Tiap Bulan</a></li>
                </ul>
            </li>
        </ul>
    </section>

</aside>
